vba (Excel unless specified)
===
Some functions written for manipulating blocks of data in excel.   I'm assuming that the data is continuous, i.e. there is a column and a header row with no null values.

get_data_width
	Returns the number of columns in your block of data, when you pass it the top left cell of the block.  Also sets the column index of the last column.

get_data_length
	Returns the number of rows in your block of data, when you pass it the top cell of an index column.  Also sets the row index of the last column.

find_column_index
	Returns the row index of a named column, when you specify a header row to search along and the number of cells to check.